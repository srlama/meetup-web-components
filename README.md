# Web Components

## Getting started
Run the startup script:
```bash
./start.sh
```

Once it is finished open up [localhost:8081](http://localhost:8081/) to see some simple hello-world examples of web components created with several different frameworks / libraries.

* Polymer
* Angular Elements
* StencilJs
* LitElement
* and off course Native JavaScript
