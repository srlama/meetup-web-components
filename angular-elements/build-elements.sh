#!/bin/sh
ng build --prod --output-hashing=none && touch dist/angular-elements/elements.js && \
  cat dist/angular-elements/runtime-es2015.js \
      dist/angular-elements/polyfills-es2015.js \
      dist/angular-elements/scripts.js \
      dist/angular-elements/main-es2015.js > dist/angular-elements/elements.js