import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularHelloComponent } from './angular-hello.component';

describe('AngularHelloComponent', () => {
  let component: AngularHelloComponent;
  let fixture: ComponentFixture<AngularHelloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularHelloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularHelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
