import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
	selector: 'app-angular-hello',
	templateUrl: './angular-hello.component.html',
	styleUrls: ['./angular-hello.component.scss'],
	encapsulation: ViewEncapsulation.ShadowDom
})
export class AngularHelloComponent implements OnInit {
	constructor() {}

	ngOnInit() {}
}
