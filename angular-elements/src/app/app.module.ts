import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AngularHelloComponent } from './angular-hello/angular-hello.component';
import { createCustomElement } from '@angular/elements';

@NgModule( {
  declarations: [AngularHelloComponent],
  entryComponents: [AngularHelloComponent],
  imports: [
    BrowserModule
  ],
  providers: []
})
export class AppModule {
  constructor( injector: Injector ) {
    const custom = createCustomElement(AngularHelloComponent, {injector});
    customElements.define('angular-elements-hello-world', custom);
  }
  ngDoBootstrap() {}}
