const typescript2 = require('rollup-plugin-typescript2');
const resolve = require('rollup-plugin-node-resolve');

const ENTRY_FILE = `src/index.ts`;

export default {
	input: ENTRY_FILE,
	output: {
		sourcemap: true,
		exports: 'named',
		file: 'dist/index.js',
		format: 'es'
	},
	plugins: [typescript2(), resolve()]
};
