console.log('TEST FROM ROLLUP');

class HelloNative extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `<h2>Hello from Native</h2>`;
	}
}

customElements.define('native-hello-world', HelloNative);
