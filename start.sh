rm -rf ./polymer/node_modules

npm i --prefix ./angular-elements
npm i --prefix ./lit-element
npm i --prefix ./stencil
npm i --prefix ./native

npm run build:elements --prefix ./angular-elements
npm run build --prefix ./lit-element
npm run build --prefix ./stencil
npm run build --prefix ./native

npm i --prefix polymer
npm run serve --prefix polymer

