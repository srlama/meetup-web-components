import { Component, h } from '@stencil/core';

@Component({
  tag: 'stencil-hello-world',
  styleUrl: 'my-component.css',
  shadow: true
})
export class MyComponent {

  render() {
    return <h2>Hello from Stencil JS</h2>;
  }
}
